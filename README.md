# swivel-chrome

## Development box set up

- git clone `https://gitlab.com/Prophesians/swivel-chrome.git`
- open `chrome://extensions` in your chrome browser
- click on _Load Unpacked_ tab
- select the _swivel-chrome_ directory from finder
- You can see _swivel_ in your plugins
- After making changes to the code, go to `chrome://extensions` and click reload button/icon on your plugin, By doing this, changes should start reflecting.