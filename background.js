'use strict';
chrome.runtime.onInstalled.addListener(function (info) {
    if (info.reason === "install") {
        const tags = prompt("Please enter your interests\n for e.g. google,machine");
        const payload = {Tags: tags.split(",")};
        let xhr = new XMLHttpRequest();
        xhr.open("POST", 'https://swivel-server.herokuapp.com/api/tags', true);

        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                const userID = JSON.parse(this.response).UserID;
                chrome.storage.sync.set({"userID": userID}, function (data) {
                    console.log("saved", data)
                });
                alert(`You have been signed up ${JSON.parse(this.response).UserID} successfully!!`)
            }
        };
        xhr.send(JSON.stringify(payload));
    }
});

let url = "";

chrome.storage.sync.get(['userID'], function (data) {
    url = `https://swivel-server.herokuapp.com/api/${data.userID}/history`;
    console.log(url);
});

const updateUserHistory = function () {
    chrome.history.search(
        {text: '', maxResults: 100},
        function (data) {
            let browserHistory = [];
            data.forEach(function (page) {
                browserHistory.push(page.title);
            });
            const xhr = new XMLHttpRequest();
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            xhr.onreadystatechange = function () {
                if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    console.log("send successfully");
                }
            };
            xhr.send(JSON.stringify({Topics: browserHistory}));
            browserHistory = [];
        }
    );
};

setInterval(function () {
    updateUserHistory();
}, 1800000);
