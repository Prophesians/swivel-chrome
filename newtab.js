let frameContainer = document.getElementById('frameContainer');
let articleURL;
chrome.storage.sync.get(['userID'], function (data) {
    let userID = data.userID;
    createProperties = {
        url: `http://swivelapp.herokuapp.com/${userID}/articles`
    };
    articleURL = `http://swivelapp.herokuapp.com/${userID}/articles`;

    frameContainer.innerHTML = `
     <iframe class="iframe" src="https://www.producthunt.com/"></iframe>
     <iframe class="iframe" src="${articleURL}"></iframe>
`;
});
