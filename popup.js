let goToWebsite = document.getElementById('goToWebsite');
let updateHistory = document.getElementById('updateHistory');
let topNews = document.getElementById('topNews');
let createProperties, articleURL, historyURL;

goToWebsite.onclick = function (element) {
    const callback = (tab) => {
        console.log(JSON.stringify(tab));
    };
    chrome.tabs.create(createProperties, callback)
};

const updateUserHistory = function () {
    chrome.history.search(
        {text: '', maxResults: 100},
        function (data) {
            let browserHistory = [];
            data.forEach(function (page) {
                browserHistory.push(page.title);
            });
            const xhr = new XMLHttpRequest();
            xhr.open("POST", historyURL, true);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            xhr.onreadystatechange = function () {
                if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    console.log("send successfully");
                }
            };
            xhr.send(JSON.stringify({Topics: browserHistory}));
            browserHistory = [];
        }
    );
};

const getFormatedInnerNews = function(rawNews){
    return rawNews.map(function(news){
       return `<div class="feature-list-item reverse">
        <div class="description"><a href="${news.Url}" target="_blank">${news.Title}</a></div>
    </div>`
}).join("")

}

const updateTopNews = function(){
    chrome.storage.sync.get(['userID'], function (data) {
        let userID = data.userID;
        createProperties = {
            url: `http://swivelapp.herokuapp.com/${userID}/articles`
        };
        historyURL = `https://swivel-server.herokuapp.com/api/${userID}/history`;
        articleURL = `https://swivel-server.herokuapp.com/api/${userID}/articles/7`;

    topNews.innerHTML = "<ul class='innerNewsUL'>";
    const xhr = new XMLHttpRequest(),
        method = "GET";

    xhr.open(method, articleURL, true);
    xhr.onreadystatechange = function () {
        let a = JSON.parse(this.response);
        if(xhr.readyState === 4 && xhr.status === 200) {
            let formatedInnerNews = getFormatedInnerNews(a.Topics);
            topNews.innerHTML = `${topNews.innerHTML}${formatedInnerNews}</ul></ul>`;
        }
    };
    xhr.send();
    });
};

updateTopNews();

updateHistory.onclick = function (element) {
    updateUserHistory()
};
